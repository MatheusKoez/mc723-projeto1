# Projeto 1 - Benchmark
### Grupo 8
* Leo Omi 138684
* Matheus Koezuka 137017
* Renato Soma 13748


## Relatório - Parte I
Imagemagick

### O que faz? Para que serve?
Imagemagick é um software open-source para edição não interativa de imagens, ou seja, editar, converter, combinar, etc via linha de comandos para manipulá-las.

### Por que é bom para medir desempenho?
Ele é bom para medir desempenho porque utiliza CPU, memória e processamento paralelo em algoritmos usados em muito dispositivos no cotidiano. No caso do nosso benchmark, estamos realizando um Gaussian Blur em uma imagem de alta resolução. Além do mais, a entrada influencia no teste, possibilitando uso de imagens de alta resolução para controlar, em parte, o tempo de execução.

### O que baixar?
Dê um clone no repositório.

### Como compilar/instalar?
O programa perf e Python devem estar previamente instalados.
Instale o ImageMagick por seu gerenciador de pacotes.

### Como executar?
Assumindo que Python já esteja instalado em sua máquina, deve ser executado o arquivo bench.py. O programa irá executar o benchmark e dar uma pontuação para a sua máquina.
Caso tenha optado por instalar o programa e suas dependências em diretório local, basta editar o arquivo run.sh para que execute o binário no diretório correto.

### Como medir o desempenho?
Para medir o desempenho será utilizado a média dos tempos de execução, a porcentagem de branch misses e porcentagem de cache misses L1 e instruções por ciclo. O programa rodará 5 vezes, uma vez que um número maior seria imprático para os propósitos desse projeto, e será utilizado média pois embora uma mediana lide melhor com valores enviesados (outliers), a média será mais precisa para uma curva mais uniforme. 

### Como apresentar o desempenho?
O desempenho será mostrado através dos seguintes itens:

*  Desempenho de tempo de execução:
(tempo base/ tempo atual)*1000

*  Desempenho do branch predictor:
((branch misses base/branch loads base) / (branch misses atual/ branch loads atual))*1000

*  Desempenho da cache:
((cache misses base/ cache loads base) / (cache misses atual/cache loads atual))*1000

*  Depempenho de instruções por cico:
(IPC base/ IPC atual)*1000

*  Pontuação geral:  
20 + (tempo base/ tempo atual)*1000 - ((branch misses atual/ branch loads atual)/ (branch misses base/branch loads base))*10 - ((cache misses atual/cache loads atual)/ (cache misses base/ cache loads base))*10

Considerando desvio padrão de 0.1 segundo em 40, o intervalo de confiança de 95% equivale a 14 pontos no benchmarking (ou seja, mais ou menos 7 a partir do valor base).

### Medições Base:
```
     125806,878142      task-clock (msec)         #    3,205 CPUs utilized            ( +-  0,23% )
             9.015      context-switches          #    0,072 K/sec                    ( +-  6,45% )
                44      cpu-migrations            #    0,000 K/sec                    ( +- 12,41% )
            39.956      page-faults               #    0,318 K/sec                    ( +-  3,40% )
   314.541.838.170      cycles                    #    2,500 GHz                      ( +-  0,29% )
   395.356.851.591      instructions              #    1,26  insns per cycle          ( +-  0,56% )
    25.980.553.796      branches                  #  206,511 M/sec                    ( +-  2,85% )
       201.920.403      branch-misses             #    0,78% of all branches          ( +-  0,02% )
    68.906.028.929      L1-dcache-loads           #  547,713 M/sec                    ( +-  1,07% )
     7.427.763.314      L1-dcache-load-misses     #   10,78% of all L1-dcache hits    ( +-  0,04% )
     2.219.875.999      LLC-loads                 #   17,645 M/sec                    ( +-  0,15% )
       471.341.904      LLC-load-misses           #   21,23% of all LL-cache hits     ( +-  1,76% )

      39,249607596 seconds time elapsed                                          ( +-  0,28% )
```