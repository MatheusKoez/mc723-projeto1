#!/usr/bin/env python 
# -*- coding: utf-8 -*-
import os

def runBench ():
    os.system("sh run.sh")

base = {"branch_misses": 201920403.0 , "branches" : 25980553796.0, "cache_misses": 7427763314.0, "cache_loads" :  68906028929.0, "time": 39.249607596, "instructions": 395356851591.0 }



def analyzeData():
    branches, branch_misses, cache_loads, cache_misses, instructions, cycles, time = 0, 0, 0, 0, 0, 0, 0
    file = open("out")
    for line in file.readlines():
        spl = line.split()
        if(len(spl) > 2):
            if(spl[1] == "branches"):
                branches = (float(spl[0].replace(",", "").replace(".", "")), spl[len(spl) - 2].replace(",", ".").replace("%", ""))
            elif(spl[1] == "branch-misses"):
                branch_misses = (float(spl[0].replace(",", "").replace(".", "")), spl[len(spl) - 2].replace(",", ".").replace("%", ""))
            elif(spl[1] == "L1-dcache-loads"):
                cache_loads = (float(spl[0].replace(",", "").replace(".", "")), spl[len(spl) - 2].replace(",", ".").replace("%", ""))
            elif(spl[1] == "L1-dcache-load-misses"):
                cache_misses = (float(spl[0].replace(",", "").replace(".", "")), spl[len(spl) - 2].replace(",", ".").replace("%", ""))
            elif(spl[1] == "instructions"):
                instructions = (float(spl[0].replace(",", "").replace(".", "")), spl[len(spl) - 2].replace(",", ".").replace("%", ""))
            elif(spl[1] == "cycles"):
                cycles = (float(spl[0].replace(",", "").replace(".", "")), spl[len(spl) - 2].replace(",", ".").replace("%", ""))
            elif(spl[1] == "seconds"):
                time = (float(spl[0].replace(",", ".")), spl[len(spl) - 2].replace(",", ".").replace("%", ""))
    return (branches[0], branch_misses[0], cache_loads[0], cache_misses[0], instructions[0], cycles[0], time[0])


def printScore(branches, branch_misses, cache_loads, cache_misses, instructions, cycles, time):
    print("Desempenho de tempo de execução: %d" % ((base["time"]/ time)*1000))
    print("Desempenho do branch predictor: %d" % (((base["branch_misses"]/base["branches"])/ (branch_misses/branches))*1000))
    print("Desempenho da cache: %d" % (((base["cache_misses"]/base["cache_loads"])/ (cache_misses/cache_loads))*1000))
    print("Pontuação geral: %d" % (20 + (base["time"]/ time)*1000 - ((branch_misses/branches)/ (base["branch_misses"]/base["branches"]))*10 - ((cache_misses/cache_loads)/ (base["cache_misses"]/base["cache_loads"]))*10))

def main ():
    runBench()
    data = analyzeData()
    printScore(*data)

if __name__ == "__main__":
    main()
